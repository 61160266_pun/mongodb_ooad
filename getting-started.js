const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')

const kittySchema = new mongoose.Schema({
  name: String
})
const Kitten = mongoose.model('Kitten', kittySchema)

const silence = new Kitten({ name: 'Silence' })
console.log(silence.name)
console.log(silence)
silence.save().then(function (result) {
  console.log(result)
}).catch(function (err) {
  console.log(err)
})
