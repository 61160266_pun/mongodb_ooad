const mongoose = require('mongoose')
const { Schema } = mongoose
const buildingSchema = Schema({
  name: String,
  floor: Number,
  rooms: [{ type: Schema.type.ObjectId, ref: 'Room' }]
})

module.exports = mongoose.model('Building', buildingSchema)
