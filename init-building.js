const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function clearDb () {
  await Room.deleteMany({})
  await Building.deleteMany({})
}

async function main () {
  await clearDb()
  const informaticsBuilding = new Building({ naem: 'Informatics', floor: 11 })
  const newInformaticsBuilding = new Building({ naem: 'New Informatics', floor: 20 })
  const room3co1 = new Room({ name: '3c01', capacity: 200, floor: 3, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3co1)
  const room4co1 = new Room({ name: '4c01', capacity: 150, floor: 4, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4co1)
  const room5co1 = new Room({ name: '5c01', capacity: 100, floor: 5, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room5co1)
  await informaticsBuilding.save()
  await newInformaticsBuilding.save()
  await room3co1.save()
  await room4co1.save()
  await room5co1.save()
}

main().then(function () {
  console.log('Finish')
})
